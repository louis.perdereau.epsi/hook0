# Hook0 — Webhook as a Service

Welcome to Hook0! Sign up to [hook0.com](https://www.hook0.com/) and start opening your SaaS to the web!

You should check our documentation website to know what Hook0 is and what is our vision: https://documentation.hook0.com/docs/what-is-hook0

# License
Hook0 is free and the source is available. Versions are published under the [Server Side Public License (SSPL) v1](./LICENSE.txt).

The license allows the free right to use, modify, create derivative works, and redistribute, with three simple limitations:

- You may not provide the products to others as a managed service
- You may not circumvent the license key functionality or remove/obscure features protected by license keys
- You may not remove or obscure any licensing, copyright, or other notices

# Find us

- [Website](https://www.hook0.com/)
- [Documentation](https://documentation.hook0.com/)
- [Status](https://status.hook0.com/)
- [Updates on Twitter](https://twitter.com/hook0_)
