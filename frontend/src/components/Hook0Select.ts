export type Hook0SelectSingleOption = {
  value: string;
  label: string;
};

export type Hook0SelectGroupedOption = {
  options: Array<Hook0SelectSingleOption>;
};
